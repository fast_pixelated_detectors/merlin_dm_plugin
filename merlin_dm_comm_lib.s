String ip = "127.0.0.1"
String port = "6341"
Number timeout = 10

String make_mpx_cmd_string(String cmd_name, String cmd_type, String cmd_arg){
	// Example string: "MPX,0000000025,SET,NUMFRAMESTOACQUIRE,1"
    String tmp_msg = "," + cmd_type + "," + cmd_name;
	if (cmd_arg != ""){
		tmp_msg = tmp_msg + "," + cmd_arg;
	}
    String cmd_len = "" + len(tmp_msg);
    // Pad number string length with zeroes as the medipix requires 10 digits
    // in the command
	while (len(cmd_len) < 10){
		cmd_len = "0" + cmd_len;
	}
    String cmd_msg = "MPX," + cmd_len + tmp_msg;
    return(cmd_msg);
}

void send_mpx_cmd_string(String send_str){
	String rtn_str;
	result(send_str + "\n");
	DM0_gwp_tcpip_send_recieve_sync_timeout( ip, port, send_str, rtn_str, timeout, "");
	result(rtn_str+"\n");
}

/////////////// Acquisition start/stop
void cmd_startacquisition(){
	String cmd_name = "STARTACQUISITION";
	String cmd_type = "CMD";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
void cmd_stopacquisition(){
	String cmd_name = "STOPACQUISITION";
	String cmd_type = "CMD";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
/////////////// Counterdepth
void set_counterdepth(Number arg){
	String cmd_name = "COUNTERDEPTH";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_counterdepth(){
	String cmd_name = "COUNTERDEPTH";
	String cmd_type = "GET";
	String cmd_arg = ""; 
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}	

/////////////// Acquisition parameters
void get_numframestoacquire(){
	String cmd_name = "NUMFRAMESTOACQUIRE";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
void set_numframestoacquire(Number arg){
	String cmd_name = "NUMFRAMESTOACQUIRE";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
////////////////////////
void get_triggerstart(){
	String cmd_name = "TRIGGERSTART";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
void set_triggerstart(Number arg){
	String cmd_name = "TRIGGERSTART";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
////////////////////////
void get_acquisitiontime(){
	String cmd_name = "ACQUISITIONTIME";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
void set_acquisitiontime(Number arg){
	String cmd_name = "ACQUISITIONTIME";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
////////////////////////
void get_acquisitionperiod(){
	String cmd_name = "ACQUISITIONPERIOD";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
void set_acquisitionperiod(Number arg){
	String cmd_name = "ACQUISITIONPERIOD";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
/////////////// CSM

void set_chargesumming(Number arg){
	String cmd_name = "CHARGESUMMING";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_chargesumming(){
	String cmd_name = "CHARGESUMMING";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

/////////////// File saving
void get_filedirectory(){
	String cmd_name = "FILEDIRECTORY";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
// Set which folder the datafiles should be saved
void set_filedirectory(String arg){
	String cmd_name = "FILEDIRECTORY";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
////////////////////////
void get_filename(){
	String cmd_name = "FILENAME";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
void set_filename(String arg){
	String cmd_name = "FILENAME";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
////////////////////////
void get_fileenable(){
	String cmd_name = "FILEENABLE";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
void set_fileenable(Number arg){
	String cmd_name = "FILEENABLE";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void set_timestamping(Number arg){
	String cmd_name = "USETIMESTAMPING";
	String cmd_type = "SET"
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_timestamping(){
	String cmd_name = "USETIMESTAMPING";
	String cmd_type = "GET"
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

///////////////// Threshold Scans
// Note that these commands perform threhold scans in terms of DAC units, not keV.
// There is another set of commands that can perform the scans in terms of keV.

void cmd_DACscan(){
	String cmd_name = "DACSCAN";
	String cmd_type = "CMD";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void set_THscan(Number arg){
	String cmd_name = "THScan";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_THscan(){
	String cmd_name = "THScan";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}
	
void set_DACscanstart(Number arg){
	String cmd_name = "DACScanStart";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_DACscanstart(){
	String cmd_name = "DACScanStart";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void set_DACscanstop(Number arg){
	String cmd_name = "DACScanStop";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_DACscanstop(){
	String cmd_name = "DACScanStop";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void set_DACscanstep(Number arg){
	String cmd_name = "DACScanStep";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_DACscanstep(){
	String cmd_name = "DACScanStep";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

////////// Threshold Control
// Commands to get and set the value of the thresholds in keV.

void set_thresholdzero(Number arg){
    String cmd_name = "THRESHOLD0";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void set_thresholdone(Number arg){
    String cmd_name = "THRESHOLD1";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);    
}

void set_thresholdtwo(Number arg){
    String cmd_name = "THRESHOLD2";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);  
}

void set_thresholdthree(Number arg){
    String cmd_name = "THRESHOLD3";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void set_thresholdfour(Number arg){
    String cmd_name = "THRESHOLD4";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);  
}

void set_thresholdfive(Number arg){
    String cmd_name = "THRESHOLD5";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);   
}

void set_thresholdsix(Number arg){
    String cmd_name = "THRESHOLD6";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);   
}

void set_thresholdseven(Number arg){
    String cmd_name = "THRESHOLD7";
	String cmd_type = "SET";
	String cmd_arg = "" + arg;
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);   
}

void get_thresholdzero(){
    	String cmd_name = "THRESHOLD0";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_thresholdone(){
    	String cmd_name = "THRESHOLD1";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_thresholdtwo(){
    	String cmd_name = "THRESHOLD2";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_thresholdthree(){
    	String cmd_name = "THRESHOLD3";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_thresholdfour(){
    	String cmd_name = "THRESHOLD4";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_thresholdfive(){
    	String cmd_name = "THRESHOLD5";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_thresholdsix(){
    	String cmd_name = "THRESHOLD6";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

void get_thresholdseven(){
    	String cmd_name = "THRESHOLD7";
	String cmd_type = "GET";
	String cmd_arg = "";
	String send_str = make_mpx_cmd_string(cmd_name, cmd_type, cmd_arg);
	send_mpx_cmd_string(send_str);
}

Class MyTestDialog: UIFrame
{
	TagGroup FieldFileDirectory, FieldFilename, FieldAcqTime, FieldAcqPeriod

	Void set_directory(Object self) set_filedirectory(FieldFileDirectory.DLGGetStringValue())
	Void set_filename(Object self) set_filename(FieldFilename.DLGGetStringValue())
	Void start_acq(Object self) cmd_startacquisition()
	Void stop_acq(Object self) cmd_stopacquisition()
	Void start_filesave(Object self) set_fileenable(1)
	Void stop_filesave(Object self) set_fileenable(0)
	Void set_acqtime(Object self) set_acquisitiontime(FieldAcqTime.DLGGetValue())
	Void set_acqperiod(Object self) set_acquisitionperiod(FieldAcqPeriod.DLGGetValue())
	Void set_internal_trigger(Object self) set_triggerstart(0)
	Void set_ttl_rising_edge_trigger(Object self) set_triggerstart(1)

	TagGroup CreateMyDialog(Object self)
		{
		TagGroup DialogTG = DLGCreateDialog("Medipix control v1")
		TagGroup Buttons, ButtonItems

		Buttons = DLGCreateBox(ButtonItems)

		TagGroup start_acq_button = DLGCreatePushButton("Start acquisition","start_acq")
		TagGroup stop_acq_button = DLGCreatePushButton("Stop acquisition","stop_acq")
		TagGroup set_acqtime_button = DLGCreatePushButton("Set acquisition time","set_acqtime")
		TagGroup set_acqperiod_button = DLGCreatePushButton("Set acquisition period","set_acqperiod")
		TagGroup set_internal_trigger_button = DLGCreatePushButton("Internal trigger","set_internal_trigger")
		TagGroup set_ttl_rising_edge_trigger_button = DLGCreatePushButton("TTL rising edge trigger","set_ttl_rising_edge_trigger")
		TagGroup directory_button = DLGCreatePushButton("Set file directory","set_directory")
		TagGroup filename_button = DLGCreatePushButton("Set file name","set_filename")
		TagGroup start_filesave_button = DLGCreatePushButton("Start filesave","start_filesave")
		TagGroup stop_filesave_button = DLGCreatePushButton("Stop filesave","stop_filesave")

		ButtonItems.DLGAddElement(start_acq_button)
		ButtonItems.DLGAddElement(stop_acq_button)
		ButtonItems.DLGAddElement(set_acqtime_button)
		ButtonItems.DLGAddElement(set_acqperiod_button)
		ButtonItems.DLGAddElement(set_internal_trigger_button)
		ButtonItems.DLGAddElement(set_ttl_rising_edge_trigger_button)
		ButtonItems.DLGAddElement(directory_button)
		ButtonItems.DLGAddElement(filename_button)
		ButtonItems.DLGAddElement(start_filesave_button)
		ButtonItems.DLGAddElement(stop_filesave_button)

		// File saving
		TagGroup Fields1, FieldItems1, Fields2, FieldItems2
		Fields1 = DLGCreateBox("File directory",FieldItems1)
		Fields2 = DLGCreateBox("Filename",FieldItems2)

		FieldFileDirectory = DLGCreateStringField("c:\\temp",30)
		FieldFileName = DLGCreateStringField("default1.bin",30)
		FieldItems1.DLGAddElement(FieldFileDirectory)
		FieldItems2.DLGAddElement(FieldFilename)

		// Acquisition parameters
		TagGroup Fields3, FieldItems3, Fields4, FieldItems4
		Fields3 = DLGCreateBox("Acquisition time (ms)",FieldItems3)
		Fields4 = DLGCreateBox("Acquisition period (ms)",FieldItems4)

		FieldAcqTime = DLGCreateIntegerField(100,10)
		FieldAcqPeriod = DLGCreateIntegerField(120,10)
		FieldItems3.DLGAddElement(FieldAcqTime)
		FieldItems4.DLGAddElement(FieldAcqPeriod)

		DialogTG.DLGAddElement(Buttons.DLGTableLayOut(2,5,0))
		DialogTG.DLGAddElement(Fields1.DLGTableLayOut(1,1,0))
		DialogTG.DLGAddElement(Fields2.DLGTableLayOut(1,1,0))
		DialogTG.DLGAddElement(Fields3.DLGTableLayOut(1,1,0))
		DialogTG.DLGAddElement(Fields4.DLGTableLayOut(1,1,0))
		Return DialogTG
		}
	Object Init(Object self) return self.super.Init(self.CreateMyDialog())
}
Object DialogOBJ = Alloc(MyTestDialog).Init()
DialogOBJ.Display("Medipix control v1")
