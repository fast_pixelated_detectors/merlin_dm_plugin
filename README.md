Digital Micrograph scripts and plugin for communicating between DM and the Merlin Medipix readout and control software, potentially allowing full control of the Medipix detector.

Note: The scripted plugins require the compiled Digital Micrograph TCP/IP communication plugin (dll) to be installed first.

Licence
-------

All files are licensed under the GPLv3, except for "gwp_dm_tcp.dll" which is free to use for non-commercial purposes.
