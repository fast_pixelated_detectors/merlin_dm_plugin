//	tcp services comms test script

result("start\n")

String ip = "127.0.0.1" //"localhost"
String port = "6341"
//String send_str = "MPX,hi,SET,Hello,from,client\n"
String send_str = "MPX,hi,GET,Hello,from,client\n"
//String send_str = "kill\n"
String rtn_str
Number timeout = 10 

// daytime
result("\n### Daytime ###\n")
DM0_gwp_tcpip_recieve_sync_timeout( ip, "13", rtn_str, timeout, "")
result(rtn_str+"\n")

// send
result("\n### Send ###\n")
String send_str2 = "Hello"
DM0_gwp_tcpip_send_sync_timeout( ip, "7", send_str2, timeout)

// echo
result("\n### Echo1 ###\n")
DM0_gwp_tcpip_send_recieve_sync_timeout( ip, "7", send_str2, rtn_str, timeout, "")
result(rtn_str+"\n")

result("\n### Echo2 ###\n")
String send_str3 = "Hello w/ newline\n"
DM0_gwp_tcpip_send_recieve_sync_timeout( ip, "7", send_str3, rtn_str, timeout, "\n")
result(rtn_str+"\n")


result("end\n")
