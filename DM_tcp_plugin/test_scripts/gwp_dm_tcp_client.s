//	merlin comms test script

result("start\n")

String ip = "127.0.0.1" //"localhost"
String port = "6341"
//String send_str = "MPX,hi,SET,Hello,from,client"	// hard coded delay
//String send_str = "MPX,hi,GET,Hello,from,client"	// instant response
String send_str = "MPX,0000000025,SET,NUMFRAMESTOACQUIRE,1"
//String send_str = "kill\n"
String rtn_str
Number timeout = 10 

// dummy merlin
result("### Talking to Dummy Merlin ###\n")
//DM0_gwp_tcpip_send_recieve_sync( ip, port, send_str, rtn_str )
DM0_gwp_tcpip_send_recieve_sync_timeout( ip, port, send_str, rtn_str, timeout, "")
result(rtn_str+"\n")

//DM0_gwp_tcpip_send_sync_timeout( ip, port, "kill", timeout)

result("end\n")

