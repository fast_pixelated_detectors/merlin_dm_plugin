class LiveImageThread : Thread{

    LiveImageThread( Object self ) { Result( "\n\t Created Thread Object id:" + self.scriptObjectGetID() ); }
    ~LiveImageThread( Object self ) { Result( "\n\t Destroyed Thread Object id:" + self.scriptObjectGetID() ); }

    Image LiveImage, line
    String PortNumber, IPaddress, SendStr, ImageName, rtn_str
    Number TimeOut, ScanSizeX, ScanSizeY, rtn_string_len, xpos, ypos
    Number receive_linesize
    Number Acquire // 0: do not acquire image. 1: acquire image
    Number BoolTest
    Number HalfLineTime // This is for avoiding the GUI freezing up

    void RunThread(Object self);

    void MyInit(Object self, Image a_img, String image_name, String port_number, String ip_address, Number sizeX, Number sizeY, Number halfline_time){
        LiveImage = a_img
        ImageName = image_name
        PortNumber = port_number
        IPaddress = ip_address
        ScanSizeX = sizeX
        ScanSizeY = sizeY
        HalfLineTime = halfline_time
        line := RealImage("",4,sizeX,1)
        TimeOut = 1
        SendStr = "BF"
        LiveImage.showImage()
        Acquire = 1
        receive_linesize = 100
    }
        
    string FetchLineFromServer(Object self){
        BoolTest = 1
        while(BoolTest == 1){
            sleep(HalfLineTime)
			DM0_gwp_tcpip_send_recieve_sync_timeout( IPaddress, PortNumber, SendStr, rtn_str, TimeOut, "")      
            if(!(rtn_str=="false")){
				if(len(rtn_str)==(receive_linesize*4)){
					BoolTest = 0
				}
			}
            if (Acquire == 0){
				BoolTest = 0
				return("null")
			}
        }
        return(rtn_str)
    } 

	void StartLiveImage(Object self){
        Acquire = 1
        xpos = 0
        ypos = 0
        
        while(Acquire == 1){
			rtn_str = self.FetchLineFromServer()
			if (rtn_str == "null"){
				Acquire = 0
			}
			else{
				for(number j=0;j<=receive_linesize-1;j++){
					if (xpos < 0){
					}
					else{
						LiveImage[xpos,ypos] = val(mid(rtn_str, j*4, 4))
					}
					if(xpos == (ScanSizeX-1)){
						xpos = 0
						ypos++
						if(ypos == (ScanSizeY)){
							ypos = 0
							// This to fix two extra frames at the end of 
							// each image
							xpos = -2
						}
					}
					else{
						xpos++
					}
					
				}
			}
        }
    } 
   
    void StopLiveImage(Object self){
        Acquire = 0
    }

    void RunThread(Object self){
        self.StartLiveImage()
    }
}

Class AcquisitionDialog : UIframe{
    AcquisitionDialog( Object self ) { Result( "\n Created DLG id:" + self.scriptObjectGetID() ); }
    ~AcquisitionDialog( Object self ) { Result( "\n Destroyed DLG id:" + self.scriptObjectGetID() ); }
        
    Image img1
    Number ScanX, ScanY, halfline_time
    String ip_address1, port_number1, image_name1
    Object livethreadobject
    
    TagGroup CreateDLG(Object self){
        ScanX = 128+1 //256
        ScanY = 128 //256
        ip_address1 = "130.209.202.100"
        //port_number1 = "6348"	// y com
        port_number1 = "6346"	// x com
        image_name1 = "Test1"
        halfline_time = 400*ScanX*0.5/1000000

        TagGroup tgItems, tg, startButton, stopButton
        tg = DLGCreateDialog( "Dialog", tgItems )
        startButton = DLGCreatePushButton( "Start", "StartLiveImage" )
        stopButton = DLGCreatePushButton( "Stop", "StopLiveImage" )
        tgItems.DLGAddElement( startButton )
        tgItems.DLGAddElement( stopButton )
        return tg
    }
    
    void StartLiveImage(Object self){
        img1 := RealImage("Test image",4,ScanX,ScanY)
        livethreadobject.MyInit(img1, image_name1, port_number1, ip_address1, ScanX, ScanY, halfline_time)
        livethreadobject.StartThread()
    }               
    
    void StopLiveImage(Object self){livethreadobject.StopLiveImage();}
    
    Object Init(Object self, Object thrdobj) {
        livethreadobject = thrdobj
        return self.super.init( self.CreateDLG() ); 
    }
}

void Main(){
    Object thrObj = Alloc(LiveImageThread)
    Object dlgObj = Alloc(AcquisitionDialog).Init(thrObj)
    dlgObj.display("Test")
}

Main()
